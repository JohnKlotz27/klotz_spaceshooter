﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public class Boundary
{
    public float xMin, xMax, zMin, zMax;
}

public class PlayerController : MonoBehaviour
{
    Rigidbody rb;
    public float speed;
    public float tilt;
    public Boundary boundary;
    public GameObject shot;
    public Transform[] shotSpawns;
    public float fireRate;
    public float FireTimer = 5f;

    
    private float nextFire;
    private AudioSource audioSource;
    private MeshCollider Phase;
    private Transform player;
    private bool speedup = true;
    private bool active = false;
    private readonly GameController score;

    public Text PrimaryFiretext;
    public Text SecondaryFiretext;
    public Text ability1text;
    public Text ability2text;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
        audioSource = GetComponent<AudioSource>();
        Phase = GetComponent<MeshCollider>();
        player = GetComponent<Transform>();

        PrimaryFiretext.text = "Left-Click Single Fire";
        SecondaryFiretext.text = "Right-Click Quintile Fire";
        ability1text.text = "Speed Buff/Debuff = C";
        ability2text.text = "Shrink/Grow = Q";


    }

    void Update()
    {
        if (Input.GetButton("Fire1") && Time.time > nextFire)
        {
            nextFire = Time.time + fireRate;
            //for(int i = 0; i < shotSpawns.Length; i++)
            Instantiate(shot, shotSpawns[0].position, shotSpawns[0].rotation);
            audioSource.Play();
            PrimaryFiretext.color = Color.Lerp(Color.red, Color.blue, Mathf.PingPong(Time.time, 1));
        }

        if (Input.GetButton("Fire2") && Time.time > nextFire)
        {
            nextFire = Time.time + fireRate;
            for (int i = 0; i < shotSpawns.Length; i++)
                Instantiate(shot, shotSpawns[i].position, shotSpawns[i].rotation);
            audioSource.Play();
            SecondaryFiretext.color = Color.Lerp(Color.red, Color.blue, Mathf.PingPong(Time.time, 1));
        }
        if (Input.GetKeyDown(KeyCode.Q))
        {
            ability2text.color = Color.Lerp(Color.red, Color.blue, Mathf.PingPong(Time.time, 1));
            if (active == false)
            {
                transform.localScale += new Vector3(-.2F, -.2F, -.2F);
                active = true;
            }
            else
            {
                transform.localScale -= new Vector3(-.2F, -.2F, -.2F);
                active = false;
            }

        }
        if (Input.GetKeyDown(KeyCode.C))
        {
            ability1text.color = Color.Lerp(Color.red, Color.blue, Mathf.PingPong(Time.time, 1));
            if (speedup == true)
            {
                speedup = false;
                speed += 10;
            }

            else
            {
                speedup = true;
                speed -= 10;
            }

        }

    }

    void FixedUpdate()
    {
        float moveHorizontal = Input.GetAxis("Horizontal");
        float moveVertical = Input.GetAxis("Vertical");

        Vector3 movement = new Vector3 (moveHorizontal, 0.0f, moveVertical);
        rb.velocity = movement * speed;

        rb.position = new Vector3
        (
            Mathf.Clamp (rb.position.x, boundary.xMin, boundary.xMax),
            0.0f,
            Mathf.Clamp (rb.position.z, boundary.zMin, boundary.zMax)
        );

        rb.rotation = Quaternion.Euler (0.0f, 0.0f, rb.velocity.x * -tilt);
    }
}
